mod page_parser;

use std::collections::HashSet;
use std::sync::Arc;
use tokio;
use reqwest;

use rdkafka::config::{ClientConfig, RDKafkaLogLevel};
use rdkafka::consumer::Consumer;
use rdkafka::consumer::stream_consumer::StreamConsumer;
use rdkafka::Message;
use rdkafka::message::ToBytes;
use rdkafka::producer::FutureProducer;
use rdkafka::producer::future_producer::FutureRecord;
use std::time;

async fn process_single_page(page_ref: &str) -> Result<Vec<String>, Box<dyn std::error::Error>> {
    let full_address = format!("https://en.wikipedia.org{}", page_ref);

    let body = reqwest::get(full_address).await?.text().await?;

    let refs = page_parser::get_page_references(body);

    let page_refs: Vec<String> = refs.into_iter().filter(|rf| {
         rf.starts_with("/wiki/") && !rf.contains(':')
    }).collect();

    return Ok(page_refs);
}

#[tokio::main]
async fn main() {
    let consumer: StreamConsumer =
        ClientConfig::new()
            .set("bootstrap.servers", "kafka:9092")
            .set("group.id", "parsers")
            .set_log_level(RDKafkaLogLevel::Debug).create().unwrap();

    let producer: std::sync::Arc<tokio::sync::Mutex<FutureProducer>> =
        std::sync::Arc::new(tokio::sync::Mutex::new(ClientConfig::new()
            .set("bootstrap.servers", "kafka:9092")
            .set_log_level(RDKafkaLogLevel::Debug).create().unwrap()));

    let (_, _) = producer.lock().await.send(
        FutureRecord::to("response").payload("empty").key("empty"),
        time::Duration::from_secs(1)).await.unwrap();
    let (_, _) = producer.lock().await.send(
        FutureRecord::to("requests").payload("empty").key("empty"),
        time::Duration::from_secs(1)).await.unwrap();

    consumer.subscribe(&["requests"]).unwrap();

    let finished = Arc::new(tokio::sync::Mutex::new(HashSet::<String>::new()));

    loop {
        let msg = consumer.recv().await.unwrap();
        if msg.key().unwrap().to_bytes() == "empty".to_bytes() {
            continue
        }
        let wiki_msg = wiki_protocol::decode(msg.payload().unwrap().to_bytes());
        {
            let mut fin = finished.lock().await;
            if wiki_msg.path.len() >= 3 || fin.contains(wiki_msg.id.as_str()) {
                continue;
            }
            if wiki_msg.next == wiki_msg.target {
                fin.insert(wiki_msg.id.clone());
                let mut next = wiki_msg.clone();
                next.path.push(wiki_msg.next.clone());
                let pr = producer.lock().await;
                let enc = wiki_protocol::encode(&next);
                let (_err, _queue_len) = pr.send(
                    FutureRecord::to("response").payload(enc.as_slice()).key("ref"),
                    time::Duration::from_secs(1)).await.unwrap();
                continue
            }
        }
        let local_producer = producer.clone();
        tokio::spawn(async move {
            let page_refs =
                process_single_page(wiki_msg.next.as_str()).await.unwrap();
            for rf in page_refs.iter() {
                if wiki_msg.path.contains(rf) || &wiki_msg.next == rf {
                    continue;
                }
                let mut next = wiki_msg.clone();
                next.next = rf.clone();
                next.path.push(wiki_msg.next.clone());
                let pr = local_producer.lock().await;
                let enc = wiki_protocol::encode(&next);
                let (_err, _queue_len) = pr.send(
                    FutureRecord::to("requests").payload(enc.as_slice()).key("ref"),
                    time::Duration::from_secs(1)).await.unwrap();
            }
        });
    }
}
