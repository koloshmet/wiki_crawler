use scraper;

pub(crate) fn get_page_references(content: String) -> Vec<String> {
    let html = scraper::Html::parse_document(content.as_str());
    let ref_selector = scraper::Selector::parse("a").unwrap();
    let references = html.select(&ref_selector);

    let mut result = vec![];
    for reference in references {
        match reference.value().attr("href") {
            Some(rf) => {
                result.push(String::from(rf))
            },
            None => {}
        }
    };
    return result;
}
