use serde::{Serialize, Deserialize};
use rmp_serde;

#[derive(Serialize, Deserialize, Clone)]
pub struct Message {
    pub id: String,
    pub next: String,
    pub target: String,
    pub path: Vec<String>
}

pub fn encode(msg: &Message) -> Vec<u8> {
    return rmp_serde::to_vec(&msg).unwrap();
}

pub fn decode(data: &[u8]) -> Message {
    return rmp_serde::from_slice::<Message>(data).unwrap();
}
