FROM rust:1.60-buster

COPY . /wiki_crawler

WORKDIR /wiki_crawler

RUN apt-get update; apt-get install -y cmake;

RUN cargo build -r

EXPOSE 1329

CMD ["sleep 10; /wiki_crawler/target/release/wiki_crawler"]
