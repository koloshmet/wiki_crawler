use std::time;
use tokio;
use tokio::io::{AsyncBufReadExt, AsyncWriteExt};

use rdkafka::config::{ClientConfig, RDKafkaLogLevel};
use rdkafka::consumer::{Consumer, StreamConsumer};
use rdkafka::Message;
use rdkafka::message::ToBytes;
use rdkafka::producer::future_producer::FutureProducer;
use rdkafka::producer::future_producer::FutureRecord;

struct CrawlerClient {
    producer: tokio::sync::Mutex<FutureProducer>,
    consumer: tokio::sync::Mutex<StreamConsumer>
}

impl CrawlerClient {
    pub fn new() -> CrawlerClient {
        return CrawlerClient {
            producer: tokio::sync::Mutex::new(ClientConfig::new()
                .set("bootstrap.servers", "kafka:9092")
                .set_log_level(RDKafkaLogLevel::Debug).create().unwrap()),
            consumer: tokio::sync::Mutex::new(ClientConfig::new()
                .set("bootstrap.servers", "kafka:9092")
                .set("group.id", "client")
                .set_log_level(RDKafkaLogLevel::Debug).create().unwrap())
        };
    }

    pub async fn process_connection(&self, socket: tokio::net::TcpStream) -> Result<(), Box<dyn std::error::Error>> {
        let mut buf = tokio::io::BufStream::new(socket);
        let mut src = String::new();
        let mut trgt = String::new();
        buf.read_line(&mut src).await?;
        buf.read_line(&mut trgt).await?;
        let msg = wiki_protocol::Message {
            id: uuid::Uuid::new_v4().to_string(),
            target: trgt.trim().to_string(),
            next: src.trim().to_string(),
            path: vec![]
        };
        println!("Connected {}", msg.id);
        let encoded = wiki_protocol::encode(&msg);
        {
            let producer = self.producer.lock().await;
            let consumer = self.consumer.lock().await;
            let (_err, _queue_len) = producer.send(
                FutureRecord::to("requests").payload(encoded.as_slice()).key("ref"),
                time::Duration::from_secs(1)).await.unwrap();
            let mut msg = consumer.recv().await.unwrap();
            if msg.key().unwrap().to_bytes() == "empty".to_bytes() {
                msg = consumer.recv().await.unwrap();
            }
            let resp = wiki_protocol::decode(msg.payload().unwrap().to_bytes());
            buf.write_all(&resp.path.join("\n").as_bytes()).await?;
        }
        buf.flush().await?;
        return Ok(());
    }
}

#[tokio::main]
async fn main() {
    let args: Vec<String> = std::env::args().collect();

    let addr: std::net::SocketAddr = format!("0.0.0.0:{}", args[1]).parse().unwrap();
    let listener = tokio::net::TcpListener::bind(addr).await.unwrap();

    let client = std::sync::Arc::new(CrawlerClient::new());
    client.consumer.lock().await.subscribe(&["response"]).unwrap();

    loop {
        let (socket, client_addr) = listener.accept().await.unwrap();
        let local_client = client.clone();
        tokio::spawn(async move {
            println!("Connection accepted from {}", client_addr);
            match local_client.process_connection(socket).await {
                Ok(_) => println!("Connection processed"),
                Err(e) => println!("Error: {}", e)
            }
        });
    }
}
